import logo from "./logo.svg";
import "./App.css";
import TodoList from "./TodoList/TodoList";
function App() {
  return (
    <div className="py-5">
      <TodoList />
    </div>
  );
}

export default App;
