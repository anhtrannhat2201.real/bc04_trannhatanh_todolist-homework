import { arrTheme } from "../../../Themes/ThemeManager";
import { ToDoListDarkTheme } from "../../../Themes/ToDoListDarkTheme";
import { ToDoListLightTheme } from "../../../Themes/ToDoListLightTheme";
import {
  ADD_TASK,
  CHANGE_THEME,
  DELETE_TASK,
  DONE_TASK,
  EDIT_TASK,
  UPDATE_TASK,
} from "../actions/types/ToDoListTypes";

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    {
      id: "task-1",
      taskName: "task 1",
      done: false,
    },
    {
      id: "task-2",
      taskName: "task 2",
      done: true,
    },
  ],
  taskEdit: { id: "-1", taskName: "", done: true },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TASK: {
      // console.log("todo", action.newTask);
      // empty test
      if (action.newTask.taskName.trim() === "") {
        alert("Task name is required!");
        return { ...state };
      }
      // check for existence
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex(
        (task) => task.taskName === action.newTask.taskName
      );
      if (index !== -1) {
        alert("Task name already exits");
        return { ...state };
      }
      taskListUpdate.push(action.newTask);
      // After processing, assign the new task list to the current task list
      state.taskList = taskListUpdate;
      return { ...state };
    }
    case CHANGE_THEME: {
      // console.log(action);
      // tìm theme dựa vào action.themeID được chọn
      let theme = arrTheme.find((theme) => theme.id == action.themeId);
      if (theme) {
        console.log(action);
        // set lại theme cho state.themeToDoList
        state.themeToDoList = { ...theme.theme };
      }
      return { ...state };
    }
    case DONE_TASK: {
      // console.log("done_task", action);
      // CLICK VÀO BUTTON CHECK=> dispatch len action có task id
      let taskListUpdate = [...state.taskList];
      // từ task id tìm ra task đó ở vị
      // trí nào trong mảng tiến hành cap nhật lại thuộc tính done=true .
      // và cập nhật lại state của redux
      let index = taskListUpdate.findIndex((task) => task.id == action.taskId);
      if (index !== -1) {
        taskListUpdate[index].done = true;
      }

      return { ...state, taskList: taskListUpdate };
    }
    case DELETE_TASK: {
      // let taskListUpdate = [...state.taskList];
      // // gán lại giá trị cho mảng taskListUpdate= chính nó
      // // nhưng filter không có taskId đó
      // taskListUpdate = taskListUpdate.filter(
      //   (task) => task.id !== action.taskId
      // );
      // // console.log(action);
      // return { ...state, taskList: taskListUpdate };
      return {
        ...state,
        taskList: state.taskList.filter((task) => task.id !== action.taskId),
      };
    }
    case EDIT_TASK: {
      return { ...state, taskEdit: action.task };
    }
    case UPDATE_TASK: {
      // console.log(action.taskName);
      //  chỉnh sửa lại taskName của taskEdit
      state.taskEdit = { ...state.taskEdit, taskName: action.taskName };
      // Tìm trong taskList cập nhật lại taskEdit người dùng update
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex(
        (task) => task.id == state.taskEdit.id
      );

      if (index !== -1) {
        console.log("index: ", index);
        taskListUpdate[index] = state.taskEdit;
      }
      state.taskList = taskListUpdate;
      state.taskEdit = { id: "-1", taskName: "", done: false };
      return { ...state };
    }
    default:
      return { ...state };
  }
};
