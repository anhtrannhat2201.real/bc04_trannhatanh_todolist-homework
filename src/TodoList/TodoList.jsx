import React, { Component } from "react";

import { ThemeProvider } from "styled-components";
import { Container } from "../Component/Container/Container";
import { ToDoListDarkTheme } from "../Themes/ToDoListDarkTheme";
import { ToDoListLightTheme } from "../Themes/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "../Themes/ToDoListPrimaryTheme";
import { Dropdown } from "../ComponentsToDoList/Dropdown";
import { Label, Input, TextField } from "../ComponentsToDoList/TextField";
import { Button } from "../ComponentsToDoList/Button";
import { Table, Tr, Td, Th, Thead, Tbody } from "../ComponentsToDoList/Table";
import {
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
} from "../ComponentsToDoList/Heading";
import { connect } from "react-redux";
import {
  ADD_TASK,
  CHANGE_THEME,
  EDIT_TASK,
} from "./redux/actions/types/ToDoListTypes";
import {
  addTaskAction,
  changeThemeAction,
  deleteTaskAction,
  doneTaskAction,
  editTaskAction,
  updateTaskAction,
} from "./redux/actions/types/ToDoListActions";
import { arrTheme } from "../Themes/ThemeManager";

class TodoList extends Component {
  state = {
    taskName: "",
    disabled: true,
  };
  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => task.done == false)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.setState(
                    {
                      disabled: false,
                    },
                    () => {
                      this.props.dispatch(editTaskAction(task));
                    }
                  );
                }}
                className="ml-1"
              >
                <i className="fa fa-edit"></i>
              </Button>

              <Button
                onClick={() => {
                  this.props.dispatch(doneTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-check"></i>
              </Button>

              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  renderTaskComplete = () => {
    return this.props.taskList
      .filter((task) => task.done == true)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  // handleChange = (e) => {
  //   let { name, value } = e.target.value;
  //   this.setState(
  //     {
  //       [name]: value,
  //     },
  //     () => {
  //       console.log(this.state);
  //     }
  //   );
  // };
  // Create renderTheme import ThemeManager
  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return <option value={theme.id}>{theme.name}</option>;
    });
  };
  // life cycle bảng 16 nhận vào pros mới được thực thi trước render
  // componentWillReceiveProps(newProps) {
  //   console.log("this.props", this.props);
  //   console.log("newProps", newProps);
  //   this.setState({
  //     taskName: newProps.taskEdit.taskName,
  //   });
  // }
  // life cycle tĩnh không truy xuất được con trỏ this
  // static getDerivedStateFromProps(newProps, currentState) {
  //   // newProps là props mới , props cũ là this.props(không truy xuất được)
  //   // currentState: ứng với state hiện tại this.state
  //   // Hoặc trả về state mới (this.state)
  //   let newState = { ...currentState, taskName: newProps.taskEdit.taskName };
  //   return newState;
  // }
  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50">
          <Dropdown
            onChange={(e) => {
              let { value } = e.target;
              // dispatch value lên reducer
              this.props.dispatch(changeThemeAction(value));
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading3>To Do List</Heading3>
          {/* <Label>TaskName</Label>
          <Input></Input> */}
          <TextField
            value={this.state.taskName}
            // onChange={(e) => {
            //   this.handleChange(e);
            // }}
            onChange={(e) => {
              this.setState(
                {
                  taskName: e.target.value,
                },
                () => {
                  console.log(this.state);
                }
              );
            }}
            name="taskName"
            label="Task name"
            className="w-50"
          />
          <Button
            onClick={() => {
              // Lay thong tin input
              let { taskName } = this.state;
              // Tao 1 task object
              let newTask = {
                id: Date.now(),
                taskName: taskName,
                done: false,
              };
              this.setState(
                {
                  taskName: "",
                },
                () => {
                  this.props.dispatch(addTaskAction(newTask));
                }
              );
              console.log(newTask);
              // Đưa task object lên redux thông qua phương thức dispatch
            }}
            className="ml-2"
          >
            <i className="fa fa-plus"></i>Add task
          </Button>
          {this.state.disabled ? (
            <Button
              disabled
              onClick={() => {
                this.props.dispatch(updateTaskAction(this.state.taskName));
              }}
              className="ml-2"
            >
              <i className="fa fa-upload"></i>Update task
            </Button>
          ) : (
            <Button
              onClick={() => {
                let { taskName } = this.state;

                this.setState(
                  {
                    disabled: true,
                    taskName: "",
                  },
                  () => {
                    this.props.dispatch(updateTaskAction(taskName));
                  }
                );
              }}
              className="ml-2"
            >
              <i className="fa fa-upload"></i>Update task
            </Button>
          )}

          <hr />
          <Heading3>Task To Do</Heading3>
          <Table>
            <Thead>{this.renderTaskToDo()}</Thead>
          </Table>
          <Heading3>Task Completed</Heading3>
          <Table>
            <Thead>{this.renderTaskComplete()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }
  // Đây là lifecycle trả về props cũ và state cũ của component trước khi render nhưng (lifecycle này sẽ được chạy sau render)
  componentDidUpdate(prevProps, prevState) {
    // so sánh nếu như props trước đó (TaskEdit trước mà khác taskEdit hiện tại thì mình mới thực hiện setState)
    if (prevProps.taskEdit.id !== this.props.taskEdit.id)
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
  }
}
const mapStateToProps = (state) => {
  return {
    themeToDoList: state.ToDoListReducer.themeToDoList,
    taskList: state.ToDoListReducer.taskList,
    taskEdit: state.ToDoListReducer.taskEdit,
  };
};
export default connect(mapStateToProps)(TodoList);
